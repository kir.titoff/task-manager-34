package ru.t1.ktitov.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

public final class DataXmlFasterXmlLoadRequest extends AbstractUserRequest {

    public DataXmlFasterXmlLoadRequest(@Nullable final String token) {
        super(token);
    }

}
