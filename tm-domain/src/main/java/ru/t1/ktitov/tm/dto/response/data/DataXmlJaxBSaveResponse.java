package ru.t1.ktitov.tm.dto.response.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.ktitov.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class DataXmlJaxBSaveResponse extends AbstractResponse {
}
