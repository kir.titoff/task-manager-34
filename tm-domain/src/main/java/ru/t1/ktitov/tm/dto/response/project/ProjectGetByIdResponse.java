package ru.t1.ktitov.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.Project;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectGetByIdResponse extends AbstractProjectResponse {

    public ProjectGetByIdResponse(@Nullable final Project project) {
        super(project);
    }

}
