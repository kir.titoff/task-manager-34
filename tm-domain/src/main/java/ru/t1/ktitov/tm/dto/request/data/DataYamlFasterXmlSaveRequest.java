package ru.t1.ktitov.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

public final class DataYamlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataYamlFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}
