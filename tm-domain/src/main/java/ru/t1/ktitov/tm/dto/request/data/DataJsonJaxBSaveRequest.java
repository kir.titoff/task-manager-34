package ru.t1.ktitov.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

public final class DataJsonJaxBSaveRequest extends AbstractUserRequest {

    public DataJsonJaxBSaveRequest(@Nullable final String token) {
        super(token);
    }

}
