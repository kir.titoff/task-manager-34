package ru.t1.ktitov.tm.exception.field;

public final class EmptyPasswordException extends AbstractFieldException {

    public EmptyPasswordException() {
        super("Error! Password is empty.");
    }

}
