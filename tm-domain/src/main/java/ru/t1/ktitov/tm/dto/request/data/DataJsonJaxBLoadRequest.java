package ru.t1.ktitov.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;

public final class DataJsonJaxBLoadRequest extends AbstractUserRequest {

    public DataJsonJaxBLoadRequest(@Nullable final String token) {
        super(token);
    }

}
