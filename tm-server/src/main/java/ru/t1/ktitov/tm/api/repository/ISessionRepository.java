package ru.t1.ktitov.tm.api.repository;

import ru.t1.ktitov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
