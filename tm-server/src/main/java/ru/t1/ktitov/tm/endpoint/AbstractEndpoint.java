package ru.t1.ktitov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.service.IServiceLocator;
import ru.t1.ktitov.tm.dto.request.AbstractUserRequest;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.user.AccessDeniedException;
import ru.t1.ktitov.tm.model.Session;

@Getter
public abstract class AbstractEndpoint {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected Session check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null || role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final Session session = serviceLocator.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new AccessDeniedException();
        if (!session.getRole().equals(role)) throw new AccessDeniedException();
        return session;
    }

    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}
